# == Class: git_user
#
# Full description of class git_user here.
#
# === Parameters
#
# Document parameters here.
#
# [*sample_parameter*]
#   Explanation of what this parameter affects and what it defaults to.
#   e.g. "Specify one or more upstream ntp servers as an array."
#
# === Variables
#
# Here you should define a list of variables that this module would require.
#
# [*sample_variable*]
#   Explanation of how this variable affects the funtion of this class and if
#   it has a default. e.g. "The parameter enc_ntp_servers must be set by the
#   External Node Classifier as a comma separated list of hostnames." (Note,
#   global variables should be avoided in favor of class parameters as
#   of Puppet 2.6.)
#
# === Examples
#
#  class { git_user:
#    servers => [ 'pool.ntp.org', 'ntp.local.company.com' ],
#  }
#
# === Authors
#
# Author Name <author@domain.com>
#
# === Copyright
#
# Copyright 2014 Your name here, unless otherwise noted.
#
class git_user {
  user { 'git':
    ensure          => 'present',
    uid             => '1001',
    home            => '/home/git',
    managehome      => true,
    purge_ssh_keys  => true,
  }

  file { '/home/git/.ssh':
    ensure          => 'directory',
    mode            => '0700',
    owner           => 'git',
    group           => 'git',
  }

  ssh_authorized_key { 'git@puppet-repo.corp.jabil.local':
    user            => 'git',
    type            => 'ssh-rsa',
    key             => 'AAAAB3NzaC1yc2EAAAABIwAAAQEA1s4RpZ6/vclUnQENkeLjJukWVeS9igKKyU/I7VB2fwAHNspKenknn6TXUV09sn4Fr+0qtPc7EeZwQ+CJelqd0ZgBb/so5u1o5zAjvqGp6v+P5TzNprpDDG+4tp/E6Aaw7UtXQEZS03wqoUJo1xW3bi5MHbbzVbA5fyyTI6CzQK6WtqTwojLDWrbJnwqY6MXwgnjtJWvm3lbkMTqeErS4JPcsle0VS9HrzN4l/76WobaK1UzJyA16Ht0CPAG3TdhYlsaBUoWs2HDfbGNTgjqS4Qz9J7e6/C3OSPvSA44LJrMYOId6ItRBZMrKmHgTDr8M8V8fHLeSEtLLNHvm27E1rQ==',
  }

}
